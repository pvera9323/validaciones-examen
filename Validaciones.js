const formulario = document.getElementById('formulario');
const inputs = document.querySelectorAll('#formulario input');
import conexion from "../../config/basedatos";
const expresiones = {

	nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, 
	apellido: /^[a-zA-ZÀ-ÿ\s]{1,40}$/,// Letras y espacios, pueden llevar acentos.
	correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
	telefono: /^\d{10}$/, // 1 a 10 números.
	cedula: /^\d{10}$/, // 1 a 10 números.
	direccion: /^[a-zA-ZÀ-ÿ\s]{1,600}$/, 
}
const campos = {
	Cedula: false,
	apellido: false,
	nombres: false,
	semestre: false,
	paralelo: false,
	correo: false,
	direccion: false,
}
const validarFormulario = (e) => {
	switch (e.target.name) {
		case "Cedula":
			validarCampo(expresiones.usuario, e.target, 'cedula');
		break;
		case "Cedula":
			validarCampo(expresiones.usuario, e.target, 'cedula');
		break;
		case "Apellido":
			validarCampo(expresiones.nombre, e.target, 'nombre');
		break;
		case "Nombre":
			validarCampo(expresiones.password, e.target, 'nombres');
		break;
		case "Semestre":
			validarCampo(expresiones.password, e.target, 'semestre');
		break;
		case "Correo":
			validarCampo(expresiones.correo, e.target, 'correo');
		break;
		case "Paralelo":
			validarCampo(expresiones.telefono, e.target, 'paralelo');
		break;
		case "Direccion":
			validarCampo(expresiones.telefono, e.target, 'direccion');
		break;
	}
}
const validarCampo = (expresion, input, campo) => {
	if(expresion.test(input.value)){
		document.getElementById(`grupo_${campo}`).classList.remove('formulario_grupo-incorrecto');
		document.getElementById(`grupo_${campo}`).classList.add('formulario_grupo-correcto');
		document.querySelector(`#grupo__${campo} i`).classList.add('fa-check-circle');
		document.querySelector(`#grupo__${campo} i`).classList.remove('fa-times-circle');
		document.querySelector(`#grupo_${campo} .formularioinput-error`).classList.remove('formulario_input-error-activo');
		campos[campo] = true;
	} else {
		document.getElementById(`grupo_${campo}`).classList.add('formulario_grupo-incorrecto');
		document.getElementById(`grupo_${campo}`).classList.remove('formulario_grupo-correcto');
		document.querySelector(`#grupo__${campo} i`).classList.add('fa-times-circle');
		document.querySelector(`#grupo__${campo} i`).classList.remove('fa-check-circle');
		document.querySelector(`#grupo_${campo} .formularioinput-error`).classList.add('formulario_input-error-activo');
		campos[campo] = false;
	}
}

export const GuardarEstudiante = async (req, res) => {

	if(Cedula!=false||apellido!=false ||nombres!=false || semestre!=false ||paralelo!=false ||correo!=false){
try {
	const { cedula, apellido,nombres,semestre, paralelo,  correo, direccion,  } = req.body;
	const newEstudent = {
		cedula,
		apellido,
		nombres,
		semestre,
		paralelo,
		correo,
		direccion

   };
	  await conexion.query(
		"INSERT INTO estudiante set ?",
		[newEstudent],
		(err) => {
		  if (!err) {
			res.json({ status: "Estudiante Guardado" });
		  } 
		}
	  );
	} catch (error) {
	  console.log(error);
	  res.json({ status: "Error en guardar estudiante" });
	}
	}
	else{
		alert("Fornulario no esta relleno");
	}
  };


inputs.forEach((input) => {
	input.addEventListener('keyup', validarFormulario);
	input.addEventListener('blur', validarFormulario);
});

formulario.addEventListener('submit', (e) => {
	e.preventDefault();
});